void main() {
  var app = new Mtn_Business_App();

  app.name = "EdiTech";
  app.category = "Educational Solution";
  app.developer = "Ambani";
  app.year = "2021";

  print("Name of the winning App is ${app.name}");
  print("The category in Best ${app.category}");
  print("It was developed by ${app.developer}");
  print("Year - ${app.year}");

  app.transformName();
}

class Mtn_Business_App {
  String name = " ";
  String? category, developer, year;

  void transformName() {
    print("App Name to capital - ${name.toUpperCase()}");
  }
}
